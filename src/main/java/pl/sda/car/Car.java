package pl.sda.car;

import java.math.BigDecimal;
import java.time.Year;

public class Car {
    private Color color;
    private BigDecimal price;
    private Year yearOfManufacture;

    public Car(Color color, BigDecimal price, Year yearOfManufacture) {
        this.color = color;
        this.price = price;
        this.yearOfManufacture = yearOfManufacture;
    }

    public Color getColor() {
        return color;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Year getYearOfManufacture() {
        return yearOfManufacture;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color=" + color +
                ", price=" + price +
                ", yearOfManufacture=" + yearOfManufacture +
                '}';
    }
}
