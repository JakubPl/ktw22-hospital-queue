package pl.sda.car;

import java.math.BigDecimal;
import java.time.Year;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        StringComparator stringComparator = new StringComparator();

        PriorityQueue<String> queue = new PriorityQueue<String>(stringComparator);
        queue.add("ala");
        queue.add("ma");
        queue.add("kota");

        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());

        MyCarComparator carComparator = new MyCarComparator();

        PriorityQueue<Car> carPriorityQueue = new PriorityQueue<>(carComparator);
        carPriorityQueue.add(new Car(Color.GREEN, BigDecimal.valueOf(150), Year.of(50)));
        carPriorityQueue.add(new Car(Color.GREEN, BigDecimal.valueOf(100), Year.of(2000)));
        carPriorityQueue.add(new Car(Color.RED, BigDecimal.valueOf(100), Year.of(1000)));
        carPriorityQueue.add(new Car(Color.RED, BigDecimal.valueOf(100000), Year.of(1001)));

        System.out.println(carPriorityQueue.poll().toString());
        System.out.println(carPriorityQueue.poll().toString());
        System.out.println(carPriorityQueue.poll().toString());
        System.out.println(carPriorityQueue.poll().toString());

    }
}
