package pl.sda.car;

import java.util.Comparator;

public class MyCarComparator implements Comparator<Car> {
    @Override
    public int compare(Car o1, Car o2) {
        //red, pozniej jak najnizsza cena
        if (o2.getColor().equals(Color.RED) && o1.getColor().equals(Color.RED)) {
            return o1.getPrice().compareTo(o2.getPrice());
        } else if (o1.getColor().equals(Color.RED)) {
            return -1;
        } else if (o2.getColor().equals(Color.RED)) {
            return 1;
        } else {
            return o1.getPrice().compareTo(o2.getPrice());
        }
    }
}
