package pl.sda.car;

import java.util.Comparator;

public class StringComparator implements Comparator<String> {
    /**
     *
     * @param o1 pierwszy obiekt do porwnania
     * @param o2 drugi obiekt do porownania
     * @return -1 jesli o1 ma wyzszy priorytet
     * 0 jesli o1 i o2 maja taki sam priorytet
     * 1 jesli o2  ma wyzszy priorytet
     */
    public int compare(String o1, String o2) {
        if(o1.length() > o2.length()) {
            return 1;
        } else if(o1.length() == o2.length()) {
            return 0;
        } else {
            return -1;
        }
    }
}
