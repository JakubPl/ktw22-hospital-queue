package pl.sda.hospital;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        String option;
        Scanner scanner = new Scanner(System.in);

        HospitalQueueService hospitalQueueService = new HospitalQueueService();
        do {
            System.out.println("a. Dodaj pacjenta\nb. Obsluz pacjenta\nc. Ilosc pacjentow\nd. Demo");
            option = scanner.nextLine();
            if ("a".equals(option)) {
                System.out.println("Obsluga przyjmowania pacjenta");
                Patient patient = handleNewPatient(scanner);
                hospitalQueueService.add(patient);
            } else if ("b".equals(option)) {
                Patient next = hospitalQueueService.next();
                String message = new StringBuilder()
                        .append("Pacjent ")
                        .append(next.getName())
                        .append(" ")
                        .append(next.getSurname())
                        .append(" zostal przyjety.")
                        .toString();
                System.out.println(message);
                System.out.println(next.toString());
            } else if ("c".equals(option)) {
                System.out.println(hospitalQueueService.count());
            } else if ("d".equals(option)) {
                for (int i = 0; i < 20; i++) {
                    hospitalQueueService.add(PatientGenerator.generate());
                }

                ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
                executor.scheduleAtFixedRate(() -> {
                            Patient generatedPatient = PatientGenerator.generate();
                            hospitalQueueService.add(generatedPatient);
                            System.out.println("Dodano");
                        }, 0, 2, TimeUnit.SECONDS);


                int i = new Random().nextInt(1001);
                executor.schedule(new ScheduledReadRunnable(hospitalQueueService, executor), i, TimeUnit.MILLISECONDS);

                /*while (true) {
                    Thread.sleep(2000);
                    hospitalQueueService.add(PatientGenerator.generate());
                    Thread.sleep(new Random().nextInt(1001));
                    hospitalQueueService.next();
                }*/
            }

        } while (!"q".equals(option));
    }

    private static Patient handleNewPatient(Scanner scanner) {
        System.out.println("Imie:");
        String name = scanner.nextLine();
        System.out.println("Nazwisko:");
        String surname = scanner.nextLine();
        System.out.println("Zlosc:");
        Integer angry = Integer.valueOf(scanner.nextLine());
        System.out.println("Choroba:");
        String stringEnumValue = scanner.nextLine();//"FLU" -> Disease.FLU
        Disease disease = Disease.valueOf(stringEnumValue);

        Patient patient = new Patient();
        patient.setName(name);
        patient.setSurname(surname);
        patient.setHowAngry(angry);
        patient.setDisease(disease);
        return patient;
    }
}
