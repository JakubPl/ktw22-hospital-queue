package pl.sda.hospital;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {

    public static final String KOWALSKI = "Kowalski";

    @Override
    public int compare(Patient o1, Patient o2) {
        boolean isO1Kowalski = KOWALSKI.equals(o1.getSurname());
        boolean isO2Kowalski = KOWALSKI.equals(o2.getSurname());

        //true i true = true
        //false i true = false
        //true i false = false
        //false i false = false


        if(!isO1Kowalski && isO2Kowalski) {
            return 1;
        } else if(isO1Kowalski && !isO2Kowalski) {
            return -1;
        } else {
            boolean isO1SthSerious = o1.getDisease().equals(Disease.STH_SERIOUS);
            boolean isO2SthSerious = o2.getDisease().equals(Disease.STH_SERIOUS);
            if(!isO1SthSerious && isO2SthSerious) {
                return 1;
            } else if(isO1SthSerious && !isO2SthSerious) {
                return -1;
            } else {
                Integer o1Factor = o1.getDisease().getInfectiousness() * o1.getHowAngry();
                Integer o2Factor = o2.getDisease().getInfectiousness() * o2.getHowAngry();
                return o2Factor.compareTo(o1Factor);
            }
        }
    }
}
