package pl.sda.hospital;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PatientGenerator {
    private static final List<String> NAMES = Arrays.asList("Jakub", "Pawel", "Szymon", "Kasia");
    private static final List<String> SURNAMES = Arrays.asList("Nowak", "Kowalski", "Karkowski", "Kociuba");

    public static Patient generate() {
        Random randomGen = new Random();
        Patient patient = new Patient();
        patient.setName(NAMES.get(randomGen.nextInt(NAMES.size())));
        patient.setSurname(SURNAMES.get(randomGen.nextInt(SURNAMES.size())));
        patient.setHowAngry(randomGen.nextInt(100));
        Disease[] diseases = Disease.values();
        int randomNumber = randomGen.nextInt(diseases.length);
        patient.setDisease(diseases[randomNumber]);

        return patient;

    }
}
