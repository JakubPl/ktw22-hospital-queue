package pl.sda.hospital;

import java.util.PriorityQueue;

public class HospitalQueueService {
    PriorityQueue<Patient> queue = new PriorityQueue<>(new PatientComparator());

    public void add(Patient patient) {
        queue.add(patient);
    }

    public int count() {
        return queue.size();
    }


    public Patient peek() {
        return queue.peek();
    }

    public Patient next() {
        return queue.remove();
    }
}
