package pl.sda.hospital;

import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledReadRunnable implements Runnable {
    private HospitalQueueService hospitalQueueService;
    private ScheduledExecutorService scheduledExecutorService;

    public ScheduledReadRunnable(HospitalQueueService hospitalQueueService, ScheduledExecutorService scheduledExecutorService) {
        this.hospitalQueueService = hospitalQueueService;
        this.scheduledExecutorService = scheduledExecutorService;
    }

    @Override
    public void run() {
        System.out.println(hospitalQueueService.next());
        int randomMilis = new Random().nextInt(1001);
        scheduledExecutorService.schedule(new ScheduledReadRunnable(hospitalQueueService, scheduledExecutorService), randomMilis, TimeUnit.MILLISECONDS);
    }
}
