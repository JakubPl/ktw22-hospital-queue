package pl.sda.hospital;

//POJO = Plain Old Java Object
public class Patient {
    private String name;
    private String surname;
    private int howAngry;
    private Disease disease;


    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setHowAngry(int howAngry) {
        this.howAngry = howAngry;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getHowAngry() {
        return howAngry;
    }

    public Disease getDisease() {
        return disease;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", howAngry=" + howAngry +
                ", disease=" + disease +
                '}';
    }
}
